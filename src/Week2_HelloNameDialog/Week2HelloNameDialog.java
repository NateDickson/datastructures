package Week2_HelloNameDialog;

import javax.swing.*;

/**
 * @author Nate Dickson
 * Date: 05/23/19
 * @version 0.0.1
 */
public class Week2HelloNameDialog {
    public static void main(String[] args) {
        String result = JOptionPane.showInputDialog(null, "What is your name?");
        JOptionPane.showMessageDialog(null,"Hello, "+ result + "!");
    }
}
