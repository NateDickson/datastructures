package Week6_Practical;
/**
 * @author Nate Dickson
 * date: 24 June 2019
 * version: 0.0.1
 * Purpose: Test the Do While Loop.
 */
public class Week6_Test_Do_While {
    public static void main(String[] args) {
        int x = 10;
        do {
            System.out.println("Value of X: " + x);
            x++;
        } while (x < 20);
    }
}
