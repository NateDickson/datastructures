package Week6_Practical;

/**
 * @author Nate Dickson
 * date: 24 June 2019
 * version: 0.0.1
 * Purpose: Test a for loop.
 */
public class Week6_Test_For {
    public static void main(String[] args) {
        for (int x = 10; x < 20; x++) {
            System.out.println("Value of X: " + x);
        }
    }
}
