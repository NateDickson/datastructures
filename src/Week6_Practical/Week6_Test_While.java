package Week6_Practical;


/**
 * @author Nate Dickson
 * Version .0.0.1
 * Date 06/24/19
 * Purpose: While Loop Example
 */
public class Week6_Test_While {
    public static void main(String[] args) {
        int x = 10;
        while (x < 20) {
            System.out.println("Value of X: " + x);
            x++;
        }
    }
}
