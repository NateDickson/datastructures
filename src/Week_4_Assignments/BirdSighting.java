package Week_4_Assignments;

public class BirdSighting {
    private int dayOfYear;
    private String species;
    private int numberOfBirds;

    public BirdSighting() {
        this.dayOfYear = 1;
        this.species = "Robin";
    }

    public BirdSighting(int dayOfYear, String species, int numberOfBirds) {
        this.dayOfYear = dayOfYear;
        this.species = species;
        this.numberOfBirds = numberOfBirds;
    }

    public int getDayOfYear() {
        return dayOfYear;
    }

    public void setDayOfYear(int dayOfYear) {
        this.dayOfYear = dayOfYear;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public int getNumberOfBirds() {
        return numberOfBirds;
    }

    public void setNumberOfBirds(int numberOfBirds) {
        this.numberOfBirds = numberOfBirds;
    }


}
