package Week_4_Assignments;

public class MathTest {
    public static void main(String[] args) {
        System.out.println("The Square root of 37 is "+ Math.sqrt(37.00));
        System.out.println("The Sine of 300 is " + Math.sin(300));
        System.out.println("The Cosine of 300 is " + Math.cos(300));
        System.out.println("The floor of 22.8 is "+ Math.floor(22.8));
        System.out.println("The ceiling of 22.8 is "+ Math.ceil(22.8));
        System.out.println("The round of 22.8 is "+ Math.round(22.8));
        System.out.println("If you compare \"D\" and 71, the larger is " + Math.max('D', 71));
        System.out.println("If you compare \"D\" and 71, the smaller is " + Math.min('D', 71));
        int randomInt = (int)(Math.random() * 20);
        System.out.println("Pick a number between 0 and 20! Did you guess ->" + randomInt + "<- ? You did? You must be MAGIC!");
    }
}
