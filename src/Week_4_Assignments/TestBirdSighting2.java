package Week_4_Assignments;

public class TestBirdSighting2 {

    public static void main(String[] args) {
        BirdSighting2 firstBs2 = new BirdSighting2();
        System.out.println("Using the default constructor we get: "+ firstBs2.humanReadableSummary());

        BirdSighting2 secondBs2 = new BirdSighting2(45, "Magpie", 32);
        System.out.println("Setting all values at construction we get " + secondBs2.humanReadableSummary());

        secondBs2.setDayOfYear(300);
        secondBs2.setSpecies("Huge Flappy Monster Thing");
        secondBs2.setNumberOfBirds(1);

        System.out.println(String.format("Using the setters and getters we get %d %s seen on day %d. And hopefully never again.",
                secondBs2.getNumberOfBirds(),
                secondBs2.getSpecies(),
                secondBs2.getDayOfYear()));
    }
}
