package Week_4_Assignments;

public class TestBirdSighting {

    public static void main(String[] args) {
        BirdSighting bs = new BirdSighting();
        System.out.println("Default date: " + bs.getDayOfYear() + "\nDefault Species: " + bs.getSpecies());

        bs.setSpecies("Raven");
        bs.setDayOfYear(360);
        bs.setNumberOfBirds(12);

        System.out.println(String.format("Now we have %d %s seen on day %d",
                bs.getNumberOfBirds(),
                bs.getSpecies(),
                bs.getDayOfYear()));
    }
}
