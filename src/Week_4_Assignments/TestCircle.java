package Week_4_Assignments;

public class TestCircle {
    public static void main(String[] args) {
        Circle smallCircle   = new Circle();
        Circle defaultCircle = new Circle();
        Circle largeCircle   = new Circle();

        smallCircle.setRadius(.5);
        largeCircle.setRadius(30);

        System.out.println("Small circle: " + smallCircle.humanReadableSummary());
        System.out.println("Default circle: " + defaultCircle.humanReadableSummary());
        System.out.println("Large Circle: " + largeCircle.humanReadableSummary());

    }

}
