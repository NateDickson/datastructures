package Week_4_Assignments;

public class Circle {
    private double radius;
    private double area;
    private double diameter;

    public Circle(){
        this.setRadius(1);
    }
    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
        calculateFromRadius();
    }

    public double getArea() {
        return area;
    }

    public double getDiameter() {
        return diameter;
    }

    private void calculateFromRadius() {
        this.diameter = this.radius*2;
        this.area = Math.PI * (this.radius*this.radius);
    }

    public String humanReadableSummary() {
        return String.format("A circle with a radius of %.2f has an area of %.2f and a diameter of %.2f",
                this.radius,
                this.area,
                this.diameter);
    }

}
