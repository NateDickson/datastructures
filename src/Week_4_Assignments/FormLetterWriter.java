package Week_4_Assignments;

public class FormLetterWriter {
    public static void main(String[] args) {
        String formLetter = "\nThank you for your recent order.";
        System.out.println(displaySalutation("Smith") +formLetter);
        System.out.println(displaySalutation("Cordwainer", "Smith") + formLetter);
    }

    private static String displaySalutation(String lastName) {
       return "Dear Mr. or Ms. "+ lastName;
    }

    private static String displaySalutation(String firstName, String lastName) {
        return String.format("Dear Mr. or Ms. %s %s", firstName, lastName);
    }
}
