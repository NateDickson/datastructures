package Week_4_Assignments;

public class Billing {
    private static double taxPercent = 0.08;
    public static void main(String[] args) {
        double bookPrice = 10.00;
        int     quantity = 5;
        double    coupon = 0.1;
        System.out.println(String.format("One book that costs $%,.2f costs $%,.2f after tax", bookPrice, computeBill(bookPrice)));
        System.out.println(String.format("%d books that cost $%,.2f each will cost $%,.2f after tax.", quantity, bookPrice, computeBill(bookPrice, quantity)));
        System.out.println(String.format("%d books that cost $%,.2f each, with a %,.2f%% coupon, will cost $%,.2f after tax.",
                quantity,
                bookPrice,
                coupon,
                computeBill(bookPrice, quantity, coupon)));
    }

    private static double computeBill(double bookPrice) {
        return addTax(bookPrice);
    }

    private static double computeBill(double bookPrice, int booksOrdered) {
        return addTax(bookPrice * booksOrdered);
    }

    /**
     * Note: in this method we're using the coupon rate as a percentage. So you could have a five percent off coupon which
     * would be represented as the double 0.05.
     * @param bookPrice the price of a single book
     * @param booksOrdered the number of books ordered.
     * @param couponPercent the percentage discount from a coupon.
     * @return the total price of the order.
     */
    private static double computeBill(double bookPrice, int booksOrdered, double couponPercent) {
        return addTax((bookPrice * booksOrdered) * (1 - couponPercent));
    }

    private static double addTax(double amount) {
        return amount * (1 + taxPercent);
    }
}
