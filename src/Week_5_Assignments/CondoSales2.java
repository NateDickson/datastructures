package Week_5_Assignments;

import java.util.Scanner;

/**
 * Figures the price of a Condo with a parking option.
 * @author Nate Dickson
 * date 06/14/19
 * This is Chapter 5 question 3b
 */
public class CondoSales2 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        double price;
        String name;

        System.out.print("Choose 1 for Park View\n2 for Golf Course View\n3 for Lake View.\n");
        int condoChoice = scanner.nextInt();

        System.out.println("Enter 1 for garage or 2 for parking space.");
        int parkingChoice = scanner.nextInt();

        switch (condoChoice) {
            case 1:
                price = 150000;
                name  = "Park View";
                break;
            case 2:
                price = 170000;
                name  = "Golf Course View";
                break;
            case 3:
                price = 210000;
                name  = "Lake View";
                break;
            default:
                price = 0;
                name  = "Invalid Selection";
        }

        if (condoChoice != 0) { //doing this one as an "if...else if...else" just for variety.
            if(parkingChoice == 1) {
                price += 5000;
                name += " with a garage";
            }
            else if (parkingChoice == 2) {
                name += " with a parking space";
            }
            else{
                name += " invalid parking selection. Using \"parking space\"";
            }
        }

        System.out.println(String.format("You chose %s which costs $%,.0f", name, price));
    }
}
