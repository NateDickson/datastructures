package Week_5_Assignments;

import java.util.Scanner;

public class Pay {

    private static double hours;
    private static int level;
    private static double regularPay = 0;
    private static double overtimePay = 0;
    private static double totalDeductions = 0;
    private static int payRate = 0;
    private static Scanner scanner;
    private static boolean hasMedical = false;
    private static boolean hasDental = false;
    private static boolean hasLongTerm = false;
    private static boolean hasRetirement = false;

    public static void main(String[] args) {
        scanner = new Scanner(System.in);
        System.out.print("Enter Skill level: ");
        level = scanner.nextInt();

        System.out.print("Enter Hours worked: ");
        hours = scanner.nextInt();

        calcPayPackage();

    }

    private static void calcPayPackage() {

        if (level == 1) payRate = 17;
        if (level == 2) payRate = 20;
        if (level == 3) payRate = 22;
        calculatePay();
        getBenefits();
        reportPay();
    }

    private static void calculatePay() {
        if (hours <= 40) {
            regularPay = hours * payRate;
        } else {
            regularPay = 40 * payRate;
            overtimePay = (hours - 40) * (payRate * 1.5);
        }
    }

    private static void getBenefits() {

        if (level > 1) {
            System.out.print("Enter 1 if the employee wants medical insurance: ");
            hasMedical = scanner.nextInt() == 1;

            System.out.print("Enter 2 if the employee wants Dental insurance: ");
            hasDental = scanner.nextInt() == 2;

            System.out.print("Enter 3 if the employee wants long-term disability insurance: ");
            hasLongTerm = scanner.nextInt() == 3;
        }
        if (level == 3) {
            System.out.print("Enter 4 if the employee wants to contribute to the retirement fund: ");
            hasRetirement = scanner.nextInt() == 4;
        }
    }

    private static void reportPay() {
        StringBuilder report = new StringBuilder();
        report.append("---------------------\n");
        report.append("Regular Pay:    $" + regularPay + "\n");
        if (overtimePay > 0) {
            report.append("Overtime Pay:   $" + overtimePay + "\n");
            report.append("Total Pay:     $" + (regularPay + overtimePay) + "\n");
            report.append("Itemized Deductions: \n");
        }
        if (hasMedical) {
            totalDeductions += 32.50;
            report.append("Medical Insurance:    $32.50\n");
        }
        if (hasDental) {
            totalDeductions += 20;
            report.append("Dental Insurance:     $20\n");
        }
        if (hasLongTerm) {
            totalDeductions += 10;
            report.append("Long-term Disability: $10\n");
        }
        if (hasRetirement) {
            double retirement = (overtimePay + regularPay) * .03;
            totalDeductions += retirement;
            report.append("Retirement account funding:    $" + retirement + "\n");
        }
        if (totalDeductions > (overtimePay + regularPay)) {
            System.out.println("ERROR: deductions are greater than pay!\n");
            System.exit(1);
        }
        if (totalDeductions > 0) {
            report.append("Total Deductions:    $" + totalDeductions + "\n");
        }
        report.append("Gross pay minus deductions: $" + (regularPay + overtimePay - totalDeductions)+"\n");
        report.append("---------------------");
        System.out.print(report.toString());
    }
}
