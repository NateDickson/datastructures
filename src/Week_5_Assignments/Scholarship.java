package Week_5_Assignments;

import java.util.Scanner;

/**
 * Evaluates a student's scholarship potential based on a combination
 * of qualifications.
 * @author Nate Dickson u0464833
 * Date 06/14/19
 *
 * Chapter 5 Exercise 4a
 */
public class Scholarship {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the student's GPA: ");
        double gpa = scanner.nextDouble();

        System.out.print("Enter the student's Extracurricular activities: ");
        int eca = scanner.nextInt();

        System.out.print("Enter the student's service activities: ");
        int service = scanner.nextInt();

        String candidacy = isACandidate(gpa, eca, service) ? "Scholarship Candidate" : "Not a candidate.";

        System.out.println(candidacy);
    }

    /**
     * Calculates a student's scholarship candidacy based on the given arguments
     * @param gpa double: represents the student's Grade Point Average.
     * @param eca int: represents the student's Extracurricular Activities.
     * @param service int: represents the student's service activities.
     * @return true or false based on the outcome of the algorithm.
     */
    private static boolean isACandidate(double gpa, int eca, int service) {
        if (gpa >= 3.8 && eca >= 1 && service >= 1) {
            return true;
        }
        if (inRange(3.4,3.8,gpa) && eca + service >= 3) {
            return true;
        }
        if (inRange(3,3.4,gpa) && eca >= 2 && service >= 3) {
            return true;
        }
        return false;
    }

    /**
     * This problem states that the gpa is in range if it's LESS THAN max but
     * GREATER THAN OR EQUAL TO min.
     * @param min the upper bound of the allowable range.
     * @param max the lower bound of the allowable range.
     * @param value the value to be tested.
     * @return true if the value is in range, false otherwise.
     */
    private static boolean inRange(double min, double max, double value) {
        if(value >= min && value < max) return true;
        return false;
    }
}
