package Week_5_Assignments;


public class TestInvoice {
    public static void main(String[] args) {
        Invoice i1 = new Invoice(42, 300, 40, 40, 40); // test all bad
        Invoice i2 = new Invoice(1042, 3000, 12, 12, 2012); //test all good

        Invoice2 i3 = new Invoice2(34, 300, 30, 22, 2017);//Everything Wrong!
        Invoice2 i4 = new Invoice2(10002, 4000, 31, 4, 2016);//too many days for April!
        Invoice2 i5 = new Invoice2(1002, 6000, 31, 10, 2017);//Just right!


        i1.displayInvoice();
        i2.displayInvoice();
        i3.displayInvoice();
        i4.displayInvoice();
        i5.displayInvoice();
    }
}
