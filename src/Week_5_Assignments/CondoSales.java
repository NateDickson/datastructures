package Week_5_Assignments;

import java.util.Scanner;

/**
 * @author Nate Dickson
 * Purpose: Return price and name of a condo.
 * Date: 06/14/19
 */
public class CondoSales {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double price;
        String name;

        System.out.print("Choose 1 for Park View\n2 for Golf Course View\n3 for Lake View.\n");
        int condoChoice = scanner.nextInt();

        switch (condoChoice) {
            case 1:
                price = 150000;
                name  = "Park View";
                break;
            case 2:
                price = 170000;
                name  = "Golf Course View";
                break;
            case 3:
                price = 210000;
                name  = "Lake View";
                break;
            default:
                price = 0;
                name  = "Invalid Selection";
        }

        System.out.println(String.format("You chose %s which costs $%,.0f", name, price));
    }
}
