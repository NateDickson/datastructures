package Week_5_Assignments;

public class DigitalCamera {
    private String brand;
    private double megapixels;
    private double price;

    public DigitalCamera(String brand, double megapixels) {
        this.brand = brand;
        this.setMegapixels(megapixels);
    }

    public String printCamera() {
        String report = "\n-----------------------------------------\n";
        report +="Brand: "+ this.brand + "\n";
        report +="megapixels: "+ this.megapixels + "\n";
        report +="price: $"+ this.price + "\n";
        report += "\n-----------------------------------------\n";

        return report;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getMegapixels() {
        return megapixels;
    }

    public void setMegapixels(double megapixels) {
        this.megapixels = megapixels > 10? 10 : megapixels;
        this.price = megapixels <= 6 ? 99 : 129;
    }

    public double getPrice() {
        return price;
    }

}
