package Week_5_Assignments;

import java.util.Scanner;

/**
 * Presents a coffee menu and tracks a customer's order.
 * @author Nate Dickson
 * Date: 06/14/19
 */
public class Coffee {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int orders = 0;
        double price = 0;
        while (orders < 3) {
            displayMenu();

            int selection = scanner.nextInt();
            if(selection == 0) break;
            switch (selection) {
                case 1:
                    price += 1.99;
                    break;
                case 2:
                    price += 2.5;
                    break;
                case 3:
                    price += 2.15;
                    break;
            }
            orders++;
        }

        System.out.println(String.format("Total price of your order: $%,.2f",price));
    }

    /**
     * Handles all the display work.
     */
    private static void displayMenu() {
        String menu =
                        "------------------------\n" +
                        "(1)American        1.99 \n" +
                        "(2)Espresso        2.50 \n" +
                        "(3)Latte           2.15 \n" +
                        "------------------------\n";
        System.out.println(menu);
        System.out.print("Enter your selection (0 to exit): ");
    }
}
