package Week_5_Assignments;

public class Invoice {
    private int invoiceNumber;
    private double balanceDue;
    private int day;
    private int month;
    private int year;

    public Invoice(int invoiceNumber, double balanceDue, int day, int month, int year) {
        this.setBalanceDue(balanceDue);
        this.setInvoiceNumber(invoiceNumber);
        this.setDay(day);
        this.setMonth(month);
        this.setYear(year);
    }

    public void displayInvoice() {
        String invoice = "\n----------------------------------\n";
        invoice += "Invoice2 #" + invoiceNumber + "\n";
        invoice += "Balance Due: $" + balanceDue + "\n";
        invoice += "Due Date: " + year + "-" + month + "-" + day;
        invoice += "\n----------------------------------\n";
        System.out.println(invoice);
    }

    public int getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(int invoiceNumber) {
        this.invoiceNumber = invoiceNumber < 1000 ? 0 : invoiceNumber;
    }

    public double getBalanceDue() {
        return balanceDue;
    }

    public void setBalanceDue(double balanceDue) {
        this.balanceDue = balanceDue;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = (day < 1 || day > 31) ? 0 : day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = (month < 1 || month > 12) ? 0 : month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = (year < 2011 || year > 2017) ? 0 : year;
    }



}
