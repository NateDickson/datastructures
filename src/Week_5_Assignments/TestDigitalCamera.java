package Week_5_Assignments;

import java.util.Scanner;

public class TestDigitalCamera {
    public static void main(String[] args) {
        DigitalCamera dc1 = setCamera();
        DigitalCamera dc2 = setCamera();
        DigitalCamera dc3 = setCamera();
        DigitalCamera dc4 = setCamera();

        System.out.println("Here are the cameras you've designed:");

        System.out.print(dc1.printCamera());
        System.out.print(dc2.printCamera());
        System.out.print(dc3.printCamera());
        System.out.print(dc4.printCamera());
    }

    private static DigitalCamera setCamera() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the Brand of the camera: ");
        String brand = scanner.nextLine();

        System.out.print("Enter the number of megapixels: ");
        double megapixels = scanner.nextDouble();

        return new DigitalCamera(brand, megapixels);
    }
}
