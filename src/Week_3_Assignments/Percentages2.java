package Week_3_Assignments;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.Scanner;

/**
 * @author Nate Dickson
 * This is Chapter 3, Question 4b
 */

public class Percentages2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the Dividend (top number): ");
        double dividend = scanner.nextDouble();
        System.out.println("Enter the Divisor (bottom number): ");
        double divisor = scanner.nextDouble();
        computePercent(dividend,divisor);
    }

    private static void computePercent(double dividend, double divisor) {
        NumberFormat format = NumberFormat.getPercentInstance(Locale.US);
        String percentage = format.format(dividend / divisor);

        System.out.println(String.format("%,.2f is %s of %,.2f", dividend, percentage, divisor));
    }
}

