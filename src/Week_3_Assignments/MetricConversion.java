package Week_3_Assignments;

import java.util.Scanner;

/**
 * @author Nate Dickson
 * This is Chapter 3, Question 6
 */
public class MetricConversion {
    private static final double CM_PER_INCH = 2.54;
    private static final double L_PER_GAL = 3.7854;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter an Integer: ");
        int input = scanner.nextInt();
        inchesToCentimeters(input);
        gallonsToLiters(input);
    }

    private static void inchesToCentimeters(int inches) {
        convertAndFormat(inches,"Inches");
    }

    private static void gallonsToLiters(int gallons) {
        convertAndFormat(gallons,"Gallons");
    }

    /**
     * Creates the converted string based on values passed from either of the other methods.
     * @param initialVal the integer we are converting from Imperial to Metric.
     * @param unitOfMeasurement a String representation of the Imperial units in question.
     */
    private static void convertAndFormat(int initialVal, String unitOfMeasurement) {
        double converted = initialVal * ((unitOfMeasurement.equals("Inches")) ? CM_PER_INCH : L_PER_GAL);
        String metricUnits = (unitOfMeasurement.equals("Inches")) ? "centimeters" : "liters";
        System.out.println(String.format("%d %s is %,.2f %s",initialVal,unitOfMeasurement,converted,metricUnits));
    }
}
