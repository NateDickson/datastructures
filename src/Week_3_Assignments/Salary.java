package Week_3_Assignments;

import java.util.Scanner;

/**
 * @author Nate Dickson
 * This is Chapter 3, Question 9
 */
public class Salary {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Please enter your hourly rate: ");
        double rate = scanner.nextDouble();

        System.out.print("Please enter the number of REGULAR hours worked: ");
        double regular = scanner.nextDouble();

        System.out.print("Please enter the number of OVERTIME hours worked: ");
        double overtime = scanner.nextDouble();

        double totalPay = calculatePayWithOvertime(rate, regular, overtime);

        System.out.println("The total paid is $" + totalPay);
    }

    private static double calculatePayWithOvertime(double rate, double regular, double overtime) {
        final double OVERTIME_RATE = 1.5;

        double regularHoursPaid = rate * regular;

        double overtimePaid = rate * OVERTIME_RATE * overtime;

        return regularHoursPaid + overtimePaid;

    }
}
