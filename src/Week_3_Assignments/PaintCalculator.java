package Week_3_Assignments;

import java.util.Scanner;

/**
 * @author Nate Dickson
 * This is Chapter 3, Question 7
 */
public class PaintCalculator {
    private static final int SQ_FEET_PER_GAL = 350;
    private static final double PRICE_PER_GAL = 32.00;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the length of the room: ");
        double length = scanner.nextDouble();

        System.out.print("Enter the width of the room: ");
        double width = scanner.nextDouble();

        System.out.print("Enter the height of the room: ");
        double height = scanner.nextDouble();

        double price = calculatePrice(height, length, width);
        String output = String.format(
                "To paint a room %.2f feet wide, %.2f long and %.2f tall will cost $%,.2f",
                width,
                length,
                height,
                price);
        System.out.println(output);
    }

    private static double calculatePrice(double height, double length, double width) {
        //four walls, two "long", two "wide"
        //so (length * height * 2) + (width * height * 2)
        //then divide by SQ_FEET_PER_GAL
        double area = (length * height * 2) + (width * height *2);

        double gallons = area/SQ_FEET_PER_GAL;
        return gallons * PRICE_PER_GAL;
    }

}
