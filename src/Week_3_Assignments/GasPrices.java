package Week_3_Assignments;

import java.util.Scanner;

/**
 * @author Nate Dickson
 * this is Chapter 3, Question 5
 */
public class GasPrices {
    private static final double MIN_COEFFICIENT = 0.035;
    private static final double MAX_COEFFICIENT = 0.04;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter price per barrel: ");
        int pricePerBarrel = scanner.nextInt();
        possiblePrices(pricePerBarrel);
    }

    private static void possiblePrices(int pricePerBarrel) {
        double minPrice = pricePerBarrel * MIN_COEFFICIENT;
        double maxPrice = pricePerBarrel * MAX_COEFFICIENT;

        System.out.println(String.format("When gasoline costs $%d per barrel\nthe price at the pump will be between $%,.2f and $%,.2f", pricePerBarrel, minPrice, maxPrice));

    }
}
