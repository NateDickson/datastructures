package Week_3_Assignments;

import java.util.Scanner;

/**
 * @author Nate Dickson
 * This is Chapter 3, Question 10
 */
public class Interest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the amount you are going to invest: ");
        double principal = scanner.nextDouble();
        double interest = calculateInterest(principal);
        System.out.println("If you invest $" + principal + ", you'll have $" + (principal+interest) + " after one year!");
    }

    /**
     * NOTE: This is working with a SINGLE compounding period per year, since the question
     * doesn't specify.
     * @param principal the amount invested.
     * @return the amount returned after 1 year.
     */
    private static double calculateInterest(double principal) {
        final double TIME = 1; //this is assigned as part of the question, oddly.
        final double RATE = .05; //also set

        return Math.pow((1 + RATE), TIME);
    }

    
}
