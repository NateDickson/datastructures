package Week_3_Assignments;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * @author Nate Dickson
 * This is Chapter 3, Question 4a
 */
public class Percentages {
    public static void main(String[] args) {
        double lower = 2;
        double higher = 5;

        computePercent(lower, higher);
        computePercent(higher,lower);

    }

    private static void computePercent(double dividend, double divisor) {
        NumberFormat format = NumberFormat.getPercentInstance(Locale.US);
        String percentage = format.format(dividend / divisor);

        System.out.println(String.format("%,.2f is %s of %,.2f", dividend, percentage, divisor));

    }
}
