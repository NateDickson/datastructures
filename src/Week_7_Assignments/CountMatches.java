package Week_7_Assignments;

import java.util.Scanner;

/**
 * @author Nate Dickson
 * date: 26 June 2019
 * version: 0.0.1
 * Purpose: Check for character matches in two strings.
 */
public class CountMatches {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String string1, string2;
        StringBuilder matches = new StringBuilder();

        System.out.print("Enter the first word: ");
        string1 = scanner.nextLine();

        System.out.print("Enter the next word: ");
        string2 = scanner.nextLine();

        for (int i = 0; i < string1.length(); i++) {
            for (int j = 0; j < string2.length(); j++) {
                if (string1.charAt(i) == string2.charAt(j)) {
                    matches.append(string1.charAt(i));
                    matches.append(" ");
                }
            }
        }
        System.out.println("Matches: " + matches.toString());

    }
}
