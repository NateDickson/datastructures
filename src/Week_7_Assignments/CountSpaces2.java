package Week_7_Assignments;

import java.util.Scanner;

/**
 * @author Nate Dickson
 * date: 26 June 2019
 * version: 0.0.1
 * Purpose: Counte the spaces in an entered string.
 */
public class CountSpaces2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input;

        System.out.print("Please type something memorable: ");
        input = scanner.nextLine();

        int spaces = 0;
        for (int i = 0; i < input.length(); i++) {
            if (Character.isWhitespace(input.charAt(i))) {
                spaces++;
            }
        }
        System.out.println("There are " + spaces + " spaces in that sentence.");
    }
}
