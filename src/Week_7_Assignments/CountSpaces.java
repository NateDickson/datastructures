package Week_7_Assignments;

/**
 * @author Nate Dickson
 * date: 26 June 2019
 * version: 0.0.1
 * Purpose: Count the spaces in a preset String
 */
public class CountSpaces {
    public static void main(String[] args) {
        String input = "The person who says something is impossible should not interrupt the person who is doing it.";
        int spaces = 0;
        for (int i = 0; i < input.length(); i++) {
            if (Character.isWhitespace(input.charAt(i))) {
                spaces++;
            }
        }
        System.out.println("There are " + spaces + " spaces in that sentence.");
    }
}
