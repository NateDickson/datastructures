package Week_7_Assignments;


import java.util.Scanner;

/**
 * @author Nate Dickson
 * date: 26 June 2019
 * version: 0.0.1
 * Purpose: Check if a string has five letters and five digits.
 */
public class FiveLettersAndFiveDigits {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean goodEnough = false;
        String submitted;

        while (!goodEnough) {
            System.out.print("Please enter a string with at least five digits and five letters: ");
            submitted = scanner.nextLine();
            goodEnough = countLettersAndDigits(5, 5, submitted);
        }

    }

    private static boolean countLettersAndDigits(int digitTarget, int letterTarget, String testString) {
        int digits  = 0;
        int letters = 0;
        for (int i=0; i <  testString.length(); i++) {
            char c = testString.charAt(i);
            if(Character.isDigit(c)){
                digits++;
            }
            if(Character.isLetter(c)){
                letters++;
            }
        }
        if (digits < digitTarget && letters < letterTarget) {
            System.out.println("You didn't enter enough of anything! Five of each! Seriously.");
            return false;
        }
        if (digits < digitTarget) {
            System.out.println("You did not enter enough digits!");
            return false;
        }
        if (letters < letterTarget) {
            System.out.println("You did not enter enough letters!");
            return false;
        }
        System.out.println("Good job! You entered enough letters and digits!");
        return true;
    }


}
