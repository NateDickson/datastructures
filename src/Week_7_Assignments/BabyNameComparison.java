package Week_7_Assignments;

import java.util.Scanner;

/**
 * @author Nate Dickson
 * date: 26 June 2019
 * version: 0.0.1
 * Purpose: Create three babynames.
 */
public class BabyNameComparison {
    public static void main(String[] args) {
        String name1, name2, name3;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the first name: ");
        name1 = scanner.nextLine();

        System.out.print("Enter the second name: ");
        name2 = scanner.nextLine();

        System.out.print("Enter the third name: ");
        name3 = scanner.nextLine();

        //three names all combos: 1-2, 1-3, 2-1, 2-3, 3-1, 3-2
        System.out.println("\nThe combinations available are: ");
        System.out.println(name1 + " " + name2);
        System.out.println(name1 + " " + name3);
        System.out.println(name2 + " " + name1);
        System.out.println(name2 + " " + name3);
        System.out.println(name3 + " " + name1);
        System.out.println(name3 + " " + name2);
    }
}
