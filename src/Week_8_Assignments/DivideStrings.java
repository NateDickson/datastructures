package Week_8_Assignments;

import java.util.Arrays;
import java.util.Scanner;


/**
 * @author Nate Dickson
 * date: 13 July 2019
 * version: 0.0.1
 * Purpose: divide strings based on length.
 */
public class DivideStrings {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] shortStrings = new String[20];
        String[] longStrings  = new String[20];

        int enteredStrings   = 0;
        int longStringCount  = 0;
        int shortStringCount = 0;

        while (enteredStrings < 20) {
            System.out.print("Enter some stuff. Press enter for a new line, up to 20 lines. Type\"quit\" to end early. ");
            String input = scanner.nextLine();

            if(input.equals("quit")) {
                enteredStrings = 30; //kill the loop.
                break;
            }

            if(input.length()> 5) {
                longStrings[longStringCount] = input;
                longStringCount++;
            }

            if (input.length() <= 5) {
                shortStrings[shortStringCount] = input;
                shortStringCount++;
            }
            enteredStrings++;
        }

        System.out.print("Type 1 to display all short strings, 2 to display all long strings: ");
        int choice = scanner.nextInt();

        if(choice == 1) printSelectedArray(shortStrings);
        if(choice == 2) printSelectedArray(longStrings);

    }

    private static void printSelectedArray(String[] selected) {
        for (int i = 0; i < selected.length; i++) {
            if (selected[i] != null) {
                System.out.println("\"" + selected[i] + "\"");
            }
        }
    }
}
