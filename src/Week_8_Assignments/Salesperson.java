package Week_8_Assignments;


/**
 * @author Nate Dickson
 * date: 13 July 2019
 * version: 0.0.1
 * Purpose: Track salesperson data.
 */
public class Salesperson {
   private int id;
   private double salesAmount;

   public Salesperson(int id, double salesAmount) {
      this.id = id;
      this.salesAmount = salesAmount;
   }


   public void printSalesperson() {
      System.out.println("----------------- ");
      System.out.println("Salesperson Data: ");
      System.out.println("Id: "+ this.id);
      System.out.println("Annual Sales Amount: $"+ this.salesAmount);
      System.out.println("----------------- ");
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public double getSalesAmount() {
      return salesAmount;
   }

   public void setSalesAmount(double salesAmount) {
      this.salesAmount = salesAmount;
   }


}
