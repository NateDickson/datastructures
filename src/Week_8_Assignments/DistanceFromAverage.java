package Week_8_Assignments;

import java.util.Scanner;

/**
 * @author Nate Dickson
 * date: 12 July 2019
 * version: 0.0.1
 * Purpose: Show how far each number is from the average of all numbers.
 */
public class DistanceFromAverage {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Double[] doubles = new Double[10];
        int entries      = 0;
        double entry     = 0;
        int actualLength = 0;
        double total     = 0;
        double average   = 0;

        while (entries < 10) {
            System.out.print("Enter up to 10 double precision numbers, 99999 to quit early: ");
            entry = scanner.nextDouble();
            if(entry == 99999) {
                entries = 11; //an easy way to short circuit the loop.
                break;
            }
            doubles[entries] = entry;
            entries++;
        }



        for (int i = 0; i < doubles.length; i++) {
            if (doubles[i] != null) {
                total += doubles[i];
                actualLength++;
            }
        }
        average = total / actualLength;

        StringBuilder output = new StringBuilder();
        output.append(String.format("Number of entries: %d, average: %.2f\n", actualLength, average));

        for (int i = 0; i < doubles.length; i++) {
            if (doubles[i] != null) {
                output.append(
                     String.format("Value: %f Distance from Average: %.2f\n",doubles[i],Math.abs(average - doubles[i]))
                );
            }
        }
        System.out.println(output.toString());
    }


}
