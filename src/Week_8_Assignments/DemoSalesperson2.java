package Week_8_Assignments;

public class DemoSalesperson2 {
    public static void main(String[] args) {

        Salesperson[] salespeople = new Salesperson[10];

        int baseId = 111;
        int baseSalesAmount = 25000;

        for (int i = 0; i < salespeople.length; i++) {
            salespeople[i] = new Salesperson(baseId + i, baseSalesAmount + (5000 * i));
        }

        for (Salesperson salesperson : salespeople) {
            salesperson.printSalesperson();
        }
    }
}
