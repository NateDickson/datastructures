package Week_8_Assignments;


/**
 * @author Nate Dickson
 * date: 13 July 2019
 * version: 0.0.1
 * Purpose: Question 5a: Demo creating a bunch of identical salespeople.
 */
public class DemoSalesperson {
    public static void main(String[] args) {

        Salesperson[] salespeople = new Salesperson[10];

        for (int i = 0; i < salespeople.length; i++) {
            salespeople[i] = new Salesperson(9999, 0);
        }

        for (Salesperson salesperson : salespeople) {
            salesperson.printSalesperson();
        }
    }
}
