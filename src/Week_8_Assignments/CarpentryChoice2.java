package Week_8_Assignments;

import java.util.Scanner;

public class CarpentryChoice2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        CarpentryProduct[] products = {
                new CarpentryProduct(250, "table"),
                new CarpentryProduct(325, "desk"),
                new CarpentryProduct(420, "dresser"),
                new CarpentryProduct(600, "entertainment center")
        };
        boolean validProduct = false;
        int price = 0;
        while (!validProduct) {
            System.out.print("Enter the name of the product to price check:");
            String entry = scanner.nextLine();
            price = checkPrice(entry, products);
            validProduct = price > 0;
        }

        System.out.println("The price is $" + price);

    }

    private static int checkPrice(String name, CarpentryProduct[] products) {
        boolean isShort = name.length()== 3;
        for (int i = 0; i < products.length; i++) {
            if (isShort && name.equals(products[i].getShortName())) {
                return products[i].getPrice();
            }
            if (!isShort && name.equals(products[i].getName())) {
                return products[i].getPrice();
            }
        }
        return -1;
    }
}
