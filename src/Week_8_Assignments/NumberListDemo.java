package Week_8_Assignments;

import java.util.Arrays;

public class NumberListDemo {
    public static void main(String[] args) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Array operations:\n");
        int[] ints = new int[8];
        double average;
        int total= 0;
        for (int i = 0; i < ints.length; i++) {//populate an array of ints.
            ints[i] = getRandomInt();
        }
        stringBuilder.append("\n1a: Initial Array: ").append(Arrays.toString(ints));
        Arrays.sort(ints);
        stringBuilder.append("\n1b: Sorted Array: ").append(Arrays.toString(ints));

        String reversed = "[";
        //yes I could just use an Arrays method here, but I won't.
        for (int i = ints.length - 1; i > 0 ; i--) reversed += ints[i] + " ";
        reversed += "]";
        stringBuilder.append("\n2: Array values in reverse: ").append(reversed);

        for (int anInt : ints) {
            total += anInt;
        }
        average = total / ints.length;
        stringBuilder.append("\n3: Sum of array values: ").append(total);
        stringBuilder.append("\n4: All values less than 5: ");

        for (int anInt : ints) if (anInt < 5) stringBuilder.append(anInt).append(" ");

        stringBuilder.append("\n5: Lowest Value in Array: ").append(ints[0]);//this will be the lowest because I sorted it.
        stringBuilder.append("\n6: Highest value in Array: ").append(ints[ints.length - 1]); //this will likewise be the highest value because of sorting!
        stringBuilder.append("\n7: Average of Array members: ").append(average);
        StringBuilder aboveAverage = new StringBuilder();
        aboveAverage.append("[");
        for( int i = 0; i< ints.length; i++) if(ints[i] > average) aboveAverage.append(ints[i]).append(" ");
        aboveAverage.append("]");

        stringBuilder.append("\n8: All values above the average: ").append(aboveAverage.toString());
        System.out.print(stringBuilder.toString());
    }

    private static int getRandomInt(){
        int min = 0;
        int max = 30;
        return min + (int)(Math.random() * ((max - min) + 1));
    }
}
