package Week_8_Assignments;

public class CarpentryProduct {
    private int price;
    private String name;
    private String shortName;

    public CarpentryProduct(int price, String name) {
        this.price = price;
        this.name = name;
        this.shortName = name.substring(0, 3);
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName(){
        return shortName;
    }
}
