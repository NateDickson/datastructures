package Week_6_Assignments;

/**
 * @author Nate Dickson
 * date: 24 June 2019
 * version: 0.0.1
 * Purpose: Calculate when the population of Mexico will pass the population of the US.
 */
public class Population {
    public static void main(String[] args) {
       double usPop   = 312000000;
       double mexPop  = 114000000;

       double mexRate = 0.0101;
       double usRate  = -0.0015;

       int year = 0;

        while (mexPop < usPop) {
            usPop  = usPop  * (1 + usRate);
            mexPop = mexPop * (1 + mexRate);
            System.out.println("Year " + year
                    + "\n  US Population: " + usPop
                    + "\n  Mexico Population: " + mexPop);
            year++;
        }
        System.out.println("Population of Mexico surpassed population of United States after " + year + " years");
    }
}
