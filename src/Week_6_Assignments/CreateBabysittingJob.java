package Week_6_Assignments;

import java.util.Scanner;

/**
 * @author Nate Dickson
 * date: 24 June 2019
 * version: 0.0.1
 * Purpose: Create a babysitting job.
 */
public class CreateBabysittingJob {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int year = 0;
        int jobCode = 0;
        int babysitter = 0;
        int children = 0;
        int hours = 0;

        while (!valid(2013, 2025, year)) {
            System.out.print("Please enter the year of this job: ");
            year = scanner.nextInt();
        }

        while (!valid(1, 9999, jobCode)) {
            System.out.print("Please enter the job code: ");
            jobCode = scanner.nextInt();
        }

        while (!valid(1, 3, babysitter)) {
            System.out.print("Please enter the babysitter's employeeId: ");
            babysitter = scanner.nextInt();
        }

        while (!valid(1, 9, children)) {
            System.out.print("Please enter the number of children to be watched: ");
            children = scanner.nextInt();
        }

        while (!valid(1, 12, hours)) {
            System.out.print("Please enter the number of hours: ");
            hours = scanner.nextInt();
        }

        BabysittingJob babysittingJob = new BabysittingJob(year, jobCode, babysitter, children, hours);

        babysittingJob.printJobDetails();
    }

    private static boolean valid(int min, int max, int value) {
        return (value >= min && value <= max);
    }
}
