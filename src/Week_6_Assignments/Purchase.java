package Week_6_Assignments;

public class Purchase {
    private int invoiceNumber;
    private double salesAmount;
    private double taxAmount;
    private final double TAX_RATE = 0.05;

    public void printDetails() {
        System.out.print("Purchase Info:\n" +
                "Invoice Number: #" + this.invoiceNumber + "\n" +
                "Net Charge:     $" + this.salesAmount + "\n" +
                "Tax:            $" + this.taxAmount + "\n" +
                "Total:          $" + (this.salesAmount + this.taxAmount));
    }

    public int getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(int invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public double getSalesAmount() {
        return salesAmount;
    }

    public void setSalesAmount(double salesAmount) {
        this.salesAmount = salesAmount;
        this.taxAmount = salesAmount * TAX_RATE;
    }

    public double getTaxAmount() {
        return taxAmount;
    }

    public double getTAX_RATE(){
        return TAX_RATE;
    }
}
