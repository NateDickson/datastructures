package Week_6_Assignments;

import java.util.Scanner;

/**
 * @author Nate Dickson
 * date: 24 June 2019
 * version: 0.0.1
 * Purpose: Create a bar graph showing cars sold per salesperson.
 */
public class BarGraph {
    public static void main(String[] args) {
        int pam, leo, kim, bob;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter cars sold by Pam: ");
        pam = scanner.nextInt();

        System.out.print("Enter cars sold by Leo: ");
        leo = scanner.nextInt();

        System.out.print("Enter cars sold by Kim: ");
        kim = scanner.nextInt();

        System.out.print("Enter cars sold by Bob: ");
        bob = scanner.nextInt();
        System.out.println("Car Sales for Month\n\n");

        System.out.println(writeGraph("Pam", pam));
        System.out.println(writeGraph("Leo", leo));
        System.out.println(writeGraph("Kim", kim));
        System.out.println(writeGraph("Bob", bob));

    }

    private static String writeGraph(String name, int sales) {
        String graph = "";
        for (int i = 0; i < sales; i++) {
            graph += "X";
        }

        return name + "  "+ graph;
    }
}
