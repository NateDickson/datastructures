package Week_6_Assignments;

import java.util.Scanner;

/**
 * @author Nate Dickson
 * date: 24 June 2019
 * version: 0.0.1
 * Purpose: Calculate the return on a terrible no-interest retirement plan.
 */
public class RetirementGoal {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int years = 0;
        while (years < 1) {
            System.out.print("Enter the number of years until retirement: ");
            years = scanner.nextInt();
        }

        double amount = 0;
        while (amount <= 0) {
            System.out.print("Enter the amount invested each year: ");
            amount = scanner.nextDouble();
        }

        double finalAmount = 0;

        for (int i = 0; i < years; i++) {
            finalAmount += amount;
        }

        System.out.println("At retirement, you will have $" + finalAmount);
    }
}
