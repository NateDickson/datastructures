package Week_6_Assignments;


/**
 * @author Nate Dickson
 * date: 24 June 2019
 * version: 0.0.1
 * Purpose: Calculate knots in more known rates of speed, from 15 to 30
 */
public class Knots {
    private static final double NAUTICAL_MILE_TO_MILE = 1.151;
    private static final double NAUTICAL_MILE_TO_KILOMETER = 1.852;
    public static void main(String[] args) {
        for (int i = 15; i <= 30; i++) {
            System.out.println(i + " knots equals:\n   "
                    + i * NAUTICAL_MILE_TO_MILE + " miles per hour\n   "
                    + i * NAUTICAL_MILE_TO_KILOMETER + " kilometers per hour.");
        }
    }
}
