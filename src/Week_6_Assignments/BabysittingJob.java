package Week_6_Assignments;

public class BabysittingJob {
    private int jobSequenceNumber;
    private int employee;
    private int children;
    private int hours;
    private int year;

    public BabysittingJob(int year, int jobSequenceNumber, int employeeNum, int children, int hours) {
       this.year = year;
       this.jobSequenceNumber = jobSequenceNumber;
       this.employee = employeeNum;
       this.children = children;
       this.hours = hours;
    }
    public void printJobDetails() {
        System.out.println("Babysitting Job Details:");
        System.out.println("--------------------------");
        System.out.println("Babysitter:   " + this.getBabysitterName());
        System.out.println("Job ID:       " + this.getJobNumber());
        System.out.println("Job Rate:    $" + this.calculateHourlyRate() + " per hour");
        System.out.println("Hours:        " + this.hours);
        System.out.println("Total Charge:$" + this.getTotalCharge());
    }
    private int calculateHourlyRate() {
        int hourly = 0;
        if (this.employee == 1) {
            hourly = 7 * children;
        }
        if (this.employee == 2 || this.employee == 3) {
            hourly = 9 + ((this.children - 1) * 4);
        }
        return hourly;
    }

    public String getBabysitterName() {
        switch (this.employee) {
            case 1:
                return "Cindy";
            case 2:
                return "Greg";
            case 3:
                return "Marcia";
            default:
                return "Please select one of the Brady Bunch.";
        }
    }

    public String getJobNumber() {
        int decadeDown = this.year - 2000;
        return String.format("%d%04d", decadeDown, this.jobSequenceNumber);
    }

    public int getTotalCharge() {
        return calculateHourlyRate() * this.hours;
    }

    public int getEmployee() {
        return employee;
    }

    public void setEmployee(int employee) {
        this.employee = employee;
    }

    public int getChildren() {
        return children;
    }

    public void setChildren(int children) {
        this.children = children;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

}
