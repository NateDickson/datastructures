package Week_6_Assignments;

import java.util.Scanner;

/**
 * @author Nate Dickson
 * date: 24 June 2019
 * version: 0.0.1
 * Purpose: Create a purchase object and test it out.
 */
public class CreatePurchase {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Purchase purchase = new Purchase();

        int invoiceNumber = 0;
        double invoiceAmount = 0;

        while (invoiceNumber < 1000 || invoiceNumber > 8000) {
            System.out.print("Please enter the invoice number (between 1000 and 8000): ");
            invoiceNumber = scanner.nextInt();
        }

        while (invoiceAmount <= 0) {
            System.out.print("Please enter the amount on the invoice: ");
            invoiceAmount = scanner.nextDouble();
        }

        purchase.setInvoiceNumber(invoiceNumber);
        purchase.setSalesAmount(invoiceAmount);
        purchase.printDetails();

    }
}
