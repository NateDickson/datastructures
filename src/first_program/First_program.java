package first_program;

import java.util.Collections;

/**
 *
 * Purpose: First example of java code.
 * @author Nate Dickson<nate.dickson@hsc.utah.edu>
 * Date: 05/14/19
 * Version: 0.0.1
 */
public class First_program {
    /**
     *
     * @param args Whatever is getting passed in.
     */
    public static void main(String[] args) {
      System.out.println(carlysMotto());
      carlysMotto2();
    }
    private static String carlysMotto(){
        return "Carlys makes the food that makes it a party.";
    }

    private static void carlysMotto2(){
        String motto = carlysMotto();
        int width  = motto.length() + 4;
        StringBuffer outString = new StringBuffer();
        StringBuffer allStarsBuf = new StringBuffer();
        for (int i = 0; i < width; i++) {
            allStarsBuf.append('*');
        }
        String allStars = allStarsBuf.toString();
        outString.append(allStars);
        outString.append('\n');
        outString.append("* ");
        outString.append(motto);
        outString.append(" *\n");
        outString.append(allStars);
        System.out.println(outString.toString());
    }

}
