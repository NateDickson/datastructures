package Week8_Array;

/**
 * @author Nate Dickson
 * date: 12 July 2019
 * version: 0.0.1
 * Purpose: Array example
 */
public class Week8_Array {
    public static void main(String[] args) {
        int[] numbers = {10,15, 20, 42, 45, 55, 61, 30};

        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }

        System.out.println("\nnow in reverse!:");

        for (int i = numbers.length - 1; i >=0 ; i--) {
            System.out.print(numbers[i] + " ");
        }
    }

}
