package Week_2_Assignments;

/**
 * @author Nate Dickson
 * This is Question 5a in chapter 2
 */
public class ProjectedSales {
    private static final double PERCENT_INCREASE = 0.1; //always name constants in ALL_CAPS, at least at my job.
    private static final String CURRENCY_FORMAT = "%,.2f";
    public static void main(String[] args) {
        double northSales = 4000;
        double southSales = 5500;
        String northProjected = String.format("%,.2f", northSales * (1 + PERCENT_INCREASE));
        String southProjected = String.format("%,.2f", southSales * (1 + PERCENT_INCREASE));

        System.out.println("Northern Region\n  Sales this year: $"+ String.format(CURRENCY_FORMAT,northSales)
                + "\n  Projected sales: $" + northProjected);

        System.out.println("Southern Region\n  Sales this year: $"+ String.format(CURRENCY_FORMAT,southSales)
                + "\n  Projected sales: $" + southProjected);

    }
}