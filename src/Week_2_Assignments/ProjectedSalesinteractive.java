package Week_2_Assignments;

import java.util.Scanner;

/**
 * @author Nate Dickson u0464833
 * Date: 05/23/19
 * This is question 5b of Chapter 2
 */
public class ProjectedSalesinteractive {

    private static final double PERCENT_INCREASE = 0.1; //always name constants in ALL_CAPS, at least at my job.
    private static final String CURRENCY_FORMAT = "%,.2f";

    public static void main(String[] args) {
        Scanner inputDevice = new Scanner(System.in);
        System.out.print("Enter sales for the Northern Region: ");
        double northSales = inputDevice.nextDouble();

        System.out.print("Enter Sales for the Southern Region: ");
        double southSales = inputDevice.nextDouble();

        printRegionSales("Northern",northSales);
        printRegionSales("Southern",southSales);
    }

    private static void printRegionSales(String region, double sales){
        String projected = String.format(CURRENCY_FORMAT, sales * (1 + PERCENT_INCREASE));
        String current   = String.format(CURRENCY_FORMAT, sales);
        System.out.println(region + "Region:\n   Sales this year: $" + current + "\n   Projected sales: $" + projected);
    }
}
