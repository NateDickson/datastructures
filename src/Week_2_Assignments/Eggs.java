package Week_2_Assignments;

import java.util.Scanner;

/**
 * @author Nate Dickson u0464833
 * This is Question 8 of Chapter 2
 */
public class Eggs {
    private static final String CURRENCY_FORMAT = "%,.2f";

    private static final double PRICE_PER_DOZ = 3.25;
    private static final double PRICE_PER_EGG = 0.45;

    public static void main(String[] args) {
        Scanner inputDevice = new Scanner(System.in);
        System.out.print("How many eggs do you need? : ");
        int orderSize = inputDevice.nextInt();

        int dozens  = orderSize / 12;
        int singles = orderSize % 12;

        double dozensTotal  = dozens  * PRICE_PER_DOZ;
        double singlesTotal = singles * PRICE_PER_EGG;
        double grandTotal   = dozensTotal + singlesTotal;

        StringBuilder explainerBuilder = new StringBuilder();

        explainerBuilder.append(String.format("You ordered %d eggs.\n", orderSize));
        explainerBuilder.append(String.format("That's %d dozens at $%,.2f per dozen, for a total of $%,.2f\n", dozens, PRICE_PER_DOZ, dozensTotal));
        explainerBuilder.append(String.format("And %d singles at $%,.2f per egg, for a total of $%,.2f\n", singles, PRICE_PER_EGG, singlesTotal));
        explainerBuilder.append(String.format("For a grand total of $%,.2f", grandTotal));

        System.out.println(explainerBuilder.toString());

    }
}
