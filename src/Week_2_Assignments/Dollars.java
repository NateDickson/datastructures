package Week_2_Assignments;

import java.util.Scanner;

/**
 * @author Nate Dickson u0464833
 * This is question 10 in Chapter 2.
 */
public class Dollars {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("How many dollars are we talkin' here? : ");

        int totalDollars = scanner.nextInt();

        int remain= totalDollars;
        int twentys   = totalDollars / 20;
        remain = totalDollars % 20;

        int tens = remain / 10;
        remain = remain % 10;

        int fives = remain / 5;
        remain = remain % 5;

        int singles = remain;

        System.out.println(String.format("Okay, that's:\n  %d Twentys\n  %d Tens\n  %d Fives\n  and %d singles.",twentys,tens,fives,singles));





    }
}
