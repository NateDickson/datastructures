package Week_2_Assignments;

/**
 * @author Nate Dickson
 * This is question 6a in Chapter 2
 */public class InchesToFeet {
    public static void main(String[] args) {
        final int inchesPerFoot = 12;
        int lengthInInches = 86;
        int feet = lengthInInches / inchesPerFoot;
        int inchesLeft = lengthInInches % inchesPerFoot;

        System.out.println(lengthInInches + " inches is "+feet + " feet and " + inchesLeft + " inches");
    }
}
