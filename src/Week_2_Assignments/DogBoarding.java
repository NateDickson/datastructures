package Week_2_Assignments;

import java.util.Scanner;

/**
 * @author  Nate Dickson
 * This is qestion 9 in Chapter 2
 */
public class DogBoarding {
    private static final double price = .5;
    public static void main(String[] args) {
        Scanner inputDevice = new Scanner(System.in);

        System.out.print("What is the name of your dog? : ");
        String dogName = inputDevice.nextLine();

        System.out.print("How much does your dog weigh? (round to nearest pound) : ");
        int pounds = inputDevice.nextInt();

        System.out.print(String.format("And how many days will %s be staying with us? : ",dogName));
        int days = inputDevice.nextInt();

        String explainer = String.format("Thank you. It will cost $%,.2f to board %s, a dog weighing %d pounds, for %d days.",
                (pounds*days*price),
                dogName,
                pounds,
                days);

        System.out.println(explainer);
    }
}
