package Week_2_Assignments;

import java.util.Scanner;

/**
 * @author Nate Dickson
 * This is question 6b in Chapter 2
 */
public class InchesToFeetInteractive {
    public static void main(String[] args) {
        final int inchesPerFoot = 12;
        Scanner inputDevice = new Scanner(System.in);

        System.out.println("Enter a length in inches");
        int lengthInInches = inputDevice.nextInt();

        int feet = lengthInInches / inchesPerFoot;
        int inchesLeft = lengthInInches % inchesPerFoot;

        System.out.println(lengthInInches + " inches is "+feet + " feet and " + inchesLeft + " inches");
    }
}
