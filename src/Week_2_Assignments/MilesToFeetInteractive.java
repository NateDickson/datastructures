package Week_2_Assignments;

import javax.swing.*;

/**
 * @author Nate Dickson
 * This is Question 4b in Chapter 2
 */
public class MilesToFeetInteractive {

    public static void main(String[] args){
        final int feetInAMile = 5280;
        String resultString = JOptionPane.showInputDialog("Enter the number of feet to Mark's house.");
        int milesToUncleMark = Integer.parseInt(resultString);
        String explainer =
                " Uncle Mark lives " +
                        milesToUncleMark +
                        " miles away, which is " +
                        milesToUncleMark * feetInAMile +
                        " feet.";
        JOptionPane.showMessageDialog(null, explainer);
    }
}
