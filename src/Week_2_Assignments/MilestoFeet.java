package Week_2_Assignments;

/**
 * @author Nate Dickson
 * @version 0.0.0
 * Purely static class.
 * This is Question 4a in Chapter 2
 */
public class MilestoFeet {
    public static void main(String[] args){
        final int feetInAMile = 5280;
        int milesToUncleMark = 357;
        String explainer =
                " My uncle Mark lives " +
                  milesToUncleMark +
                " miles away, which is " +
                  milesToUncleMark * feetInAMile +
                " feet.";
        System.out.println(explainer);
    }
}
