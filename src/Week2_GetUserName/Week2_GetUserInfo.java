package Week2_GetUserName;

import java.util.Scanner;

/**
 *
 * @author Nate Dickson
 * Purpose: Accepts values from the User.
 * Date: 05/23/19
 * @version 0.0.1
 */
public class Week2_GetUserInfo {

    public static void main(String[] args){
        Scanner inputDevice = new Scanner(System.in);
        System.out.print("Please enter your name:");
        String name = inputDevice.nextLine();
        System.out.print("Please enter your age:");
        int age = inputDevice.nextInt();
        System.out.println("Your name is " + name + " and you are "+ age + "years old");
    }
}
