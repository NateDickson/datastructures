package Week7_CompareStrings;

import java.util.Scanner;

/**
 * @author Nate Dickson
 * date: 26 June 2019
 * version: 0.0.1
 * Purpose: Compare two strings. I simplified a bit from the original.
 */
public class Week7_CompareStrings {
    public static void main(String[] args) {
        String firstName = "Nate";
        String otherName;
        System.out.print("Please enter your name: ");
        Scanner scanner = new Scanner(System.in);
        otherName = scanner.nextLine();
        String equality = (firstName.equals(otherName)) ? " equals " : " does not equal ";

        System.out.println(firstName + equality + otherName);
    }
}
