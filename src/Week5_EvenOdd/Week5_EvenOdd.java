package Week5_EvenOdd;

import java.util.Scanner;

/**
 * @author Nate Dickson
 * Purpose :determine if a number is even or odd.
 *
 */
public class Week5_EvenOdd {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Please enter a number: ");
        int number = input.nextInt();

        if (isEven(number)) {
            System.out.println(number + " is even.");
        }else{
            System.out.println(number + " is odd.");
        }

    }

    private static boolean isEven(int number) {
       boolean result;
        if (number % 2 == 0) {
            result = true;
        }else{
            result = false;
        }
        return result;
    }
}
