package FinalExam;

import java.util.Scanner;

/**
 * @author Nate Dickson
 * date: 25 July 2019
 * version: 0.0.1
 * Purpose: Compute the average of five numbers
 */

public class Average {
    public static void main(String[] args) {
        int[] numbers = new int[5];
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter five integers: ");

        for (int i = 0; i <numbers.length; i++) {
           numbers[i] = scanner.nextInt();
        }
        int total = 0;
        for (int i = 0; i < numbers.length; i++) {
            total +=numbers[i];
        }

        System.out.print("The average is " + (total / numbers.length));

    }
}
