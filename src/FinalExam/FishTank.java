package FinalExam;

import java.util.Scanner;

/**
 * @author Nate Dickson
 * date: 25 July 2019
 * version: 0.0.1
 * Purpose: Test fish tank pH.
 */

public class FishTank {
    public static void main(String[] args) {
        double pH = -1;
        Scanner scanner = new Scanner(System.in);
        while (pH < 0 || pH > 14) {
            System.out.print("Please enter the pH of the fishtank: (between 0 and 14): ");
            pH = scanner.nextDouble();
        }
        String output = "The water in the fishtank is ";
        if (pH >= 0 && pH < 7) {
            output += "acidic.";
        } else if (pH == 7) {
            output += "neutral";
        }else{
            output += "basic";
        }

        System.out.print(output);
    }
}
