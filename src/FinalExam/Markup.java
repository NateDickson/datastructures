package FinalExam;

import java.util.Scanner;

/**
 * @author Nate Dickson
 * date: 25 July 2019
 * version: 0.0.1
 * Purpose: Show the price after various markup amounts.
 */

public class Markup {
    public static void main(String[] args) {
        double price = 0;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the wholesale price: ");
        price = scanner.nextDouble();
        StringBuilder sb = new StringBuilder();
        sb.append("Original price: $");
        sb.append(price);
        for (int markup = 5; markup < 11; markup++) {
           sb.append("\nPrice after ");
            sb.append(markup);
            sb.append("%: $");
            sb.append(price * (1 + (markup * .01)));
        }
        System.out.print(sb.toString());
    }
}
