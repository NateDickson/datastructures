package FinalExam;

/**
 * @author Nate Dickson
 * date: 25 July 2019
 * version: 0.0.1
 * Purpose: Test the student class
 */

public class TestStudent {
    public static void main(String[] args) {
        Student student = new Student();
        student.setFirstName("Nate");
        student.setLastName("Dickson");
        student.setGPA(3.89);

        displayStudent(student);
    }

    private static void displayStudent(Student student) {
        StringBuilder sb = new StringBuilder();
        sb.append("Student Name: ");
        sb.append(student.getFirstName());
        sb.append(" ");
        sb.append(student.getLastName());
        sb.append("\n");
        sb.append("GPA: ");
        sb.append(student.getGPA());
        System.out.print(sb.toString());
    }

}
