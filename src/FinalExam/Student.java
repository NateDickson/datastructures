package FinalExam;

/**
 * @author Nate Dickson
 * date: 25 July 2019
 * version: 0.0.1
 * Purpose: Contains information about a student.
 */
public class Student {
    private String studentNumber;
    private String firstName;
    private String lastName;
    private double GPA;
    private String major;

    public Student(){

    }

    public Student(String studentNumber, String firstName, String lastName, String major, double GPA) {
        this.firstName = firstName;
        this.studentNumber = studentNumber;
        this.lastName = lastName;
        this.major = major;
        this.GPA = GPA;
    }

    public String getStudentNumber() {
        return studentNumber;
    }

    public void setStudentNumber(String studentNumber) {
        this.studentNumber = studentNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getGPA() {
        return GPA;
    }

    public void setGPA(double GPA) {
        this.GPA = GPA;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }



}
