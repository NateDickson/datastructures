package Week4_Employee;

/**
 * @author Nate Dickson
 */
public class Employee {
    private int empNum;
    private String empLastName;
    private String empFirstName;
    private double empSalary;

    public void setEmpNum(int emp) {
        empNum = emp;
    }

    public int getEmpNum() {
        return empNum;
    }

    public void setEmpLastName(String lastName) {
        empLastName = lastName;
    }

    public String getEmpLastName() {
        return empLastName;
    }

    public void setEmpFirstName(String firstName) {
        empFirstName = firstName;
    }

    public String getEmpFirstName() {
        return empFirstName;
    }

    public void setEmpSalary(double empSalary) {
        this.empSalary = empSalary;
    }

    public double getEmpSalary() {
        return empSalary;
    }

}
