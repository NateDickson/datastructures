package Week4_Employee;


/**
 * @author Nate Dickson
 * Purpose: First Class example
 * version : 0.0.1
 * Date: 06/01/19
 */
public class Week4_Employee {

    public static void main(String[] args) {
        Employee e1 = new Employee();
        Employee e2 = new Employee();

        e1.setEmpNum(42);
        e1.setEmpLastName("Adams");
        e1.setEmpFirstName("Douglas");
        e1.setEmpSalary(75000);

        e2.setEmpNum(4098);
        e2.setEmpLastName("Pratchett");
        e2.setEmpFirstName("Terry");
        e2.setEmpSalary(84000);

        System.out.println("The output of my objects are: ");
        System.out.println(e1.getEmpNum() + " Last Name: " + e1.getEmpLastName() + " First Name: "+e1.getEmpFirstName()+ "Salary: $"+e1.getEmpSalary());
        System.out.println(e2.getEmpNum() + " Last Name: " + e2.getEmpLastName() + " First Name: "+e2.getEmpFirstName());

    }
}
