package Week3_DemoRaise2;

/**
 * @author NateDickson
 * Purpose: More Raise calculation
 */
public class Week3_DemoRaise2 {
    public static void main(String[] args) {
        double salary = 200.00;
        System.out.println("Demonstrating some raises.");
        System.out.println("The Output is: " + predictRaise(salary));
    }

    public static double predictRaise(double salary) {
//        double newSalary;
        double totalAmount;
        final double RAISE = 1.10;
//        newSalary = salary * RAISE;
        totalAmount = calBonus(salary * RAISE);
        return totalAmount;
    }

    public static double calBonus(double salary){
        double totalAmount = salary + 100;

        return totalAmount;
    }
}
