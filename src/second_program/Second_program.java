package second_program;

import javax.swing.JOptionPane;

/**
 *
 * Purpose: Second example of java code.
 * @author Nate Dickson<nate.dickson@hsc.utah.edu>
 * Date: 05/14/19
 * Version: 0.0.1
 */
public class Second_program {
    public static void main(String args[]) {
        JOptionPane.showMessageDialog(null,"First Java Dialog");
    }
}
