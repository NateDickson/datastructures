package Week3_DemoRaise1;

/**
 * @author Nate Dickson
 * Purpose: Created a method that calucates a raise.
 *
 */
public class Week3_DemoRaise1 {
    public static void main(String[] args) {
        double salary = 200.00;
        double startingWage = 800.00;
        System.out.println("Demonstrating some raises.");
        predictRaise(salary);
        predictRaise(startingWage);
    }

    public static void predictRaise(double sal){
        double newSalary;
        final double RAISE = 1.10;
        newSalary = sal * RAISE;
        System.out.println("Current Salary: $" + sal + " After Raise: $" + newSalary);
    }
}
