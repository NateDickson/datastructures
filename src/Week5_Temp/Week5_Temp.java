package Week5_Temp;

import java.util.Scanner;

/**
 * @author Nate Dickson
 * date: 06/11/19
 * Purpose: temp warning.
 */
public class Week5_Temp {
    public static void main(String[] args) {
       int high, low, diff;
       final int HIGH_THRESHOLD = 90;
       final int LOW_THRESHOLD  = 32;
       final int DIFF_THRESHOLD = 40;

        Scanner input = new Scanner(System.in);

        System.out.print("Enter the high temperature: ");
        high = input.nextInt();

        System.out.print("Enter the low temp for the day: ");
        low = input.nextInt();

        if (high >= HIGH_THRESHOLD) {
            System.out.println("Heat warning!");
        }
        if (low <= LOW_THRESHOLD) {
            System.out.println("Freeze Warning!");
        }

        if (high - low >= DIFF_THRESHOLD) {
            System.out.println("Large temperature swing!");
        }
    }
}
